package week6

case class Book(title: String, authors: List[String])

object maps {

  val capitalOfCountry = Map("US" -> "Washington", "Switzerland" -> "Bern")
                                                  //> capitalOfCountry  : scala.collection.immutable.Map[String,String] = Map(US -
                                                  //| > Washington, Switzerland -> Bern)
  val countryOfCapital = capitalOfCountry map {
    case (x, y) => (y, x)
  }                                               //> countryOfCapital  : scala.collection.immutable.Map[String,String] = Map(Wash
                                                  //| ington -> US, Bern -> Switzerland)

  capitalOfCountry("US")                          //> res0: String = Washington

  capitalOfCountry get "US"                       //> res1: Option[String] = Some(Washington)
  capitalOfCountry get "Andorra"                  //> res2: Option[String] = None

  capitalOfCountry.withDefaultValue("unknown").get("Andorra")
                                                  //> res3: Option[String] = None

  capitalOfCountry get "US" getOrElse "Nu stiu"   //> res4: String = Washington

  val fruit = List("apple", "pear", "orange", "pineapple")
                                                  //> fruit  : List[String] = List(apple, pear, orange, pineapple)
  fruit sortWith (_.length < _.length)            //> res5: List[String] = List(pear, apple, orange, pineapple)
  fruit.sorted                                    //> res6: List[String] = List(apple, orange, pear, pineapple)
  
  fruit.groupBy(_.head)                           //> res7: scala.collection.immutable.Map[Char,List[String]] = Map(p -> List(pear
                                                  //| , pineapple), a -> List(apple), o -> List(orange))

  val books: List[Book] = List(
    Book(title = "Structure and Interpretation of Computer Programs",
      authors = List("Abelson, Harald", "Sussman, Gerald J.")),
    Book(title = "Introduction to Functional Programming",
      authors = List("Bird, Richard", "Wadler, Phil")),
    Book(title = "Effective Java",
      authors = List("Bloch, Joshua")),
    Book(title = "Java Puzzlers",
      authors = List("Bloch, Joshua", "Gafter, Neal")),
    Book(title = "Programming in Scala",
      authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill")))
                                                  //> books  : List[week6.Book] = List(Book(Structure and Interpretation of Compu
                                                  //| ter Programs,List(Abelson, Harald, Sussman, Gerald J.)), Book(Introduction 
                                                  //| to Functional Programming,List(Bird, Richard, Wadler, Phil)), Book(Effectiv
                                                  //| e Java,List(Bloch, Joshua)), Book(Java Puzzlers,List(Bloch, Joshua, Gafter,
                                                  //|  Neal)), Book(Programming in Scala,List(Odersky, Martin, Spoon, Lex, Venner
                                                  //| s, Bill)))

  for {
    b <- books
    a <- b.authors
    if a startsWith "Bird,"
  } yield b.title                                 //> res8: List[String] = List(Introduction to Functional Programming)

  books.flatMap(
    b => b.authors.withFilter(
      a => a startsWith "Bird,").map(
        a => b.title))                            //> res9: List[String] = List(Introduction to Functional Programming)

  val bookSet = books.toSet                       //> bookSet  : scala.collection.immutable.Set[week6.Book] = Set(Book(Programmin
                                                  //| g in Scala,List(Odersky, Martin, Spoon, Lex, Venners, Bill)), Book(Structur
                                                  //| e and Interpretation of Computer Programs,List(Abelson, Harald, Sussman, Ge
                                                  //| rald J.)), Book(Effective Java,List(Bloch, Joshua)), Book(Introduction to F
                                                  //| unctional Programming,List(Bird, Richard, Wadler, Phil)), Book(Java Puzzler
                                                  //| s,List(Bloch, Joshua, Gafter, Neal)))
  for {
    b1 <- bookSet
    b2 <- bookSet
    if b1 != b2
    a1 <- b1.authors
    a2 <- b2.authors
    if a1 == a2
  } yield a1                                      //> res10: scala.collection.immutable.Set[String] = Set(Bloch, Joshua)

}