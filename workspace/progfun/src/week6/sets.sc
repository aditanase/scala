package week6

object sets {

  def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double =
    (xs zip ys).map(xy => xy._1 * xy._2).sum      //> scalarProduct: (xs: Vector[Double], ys: Vector[Double])Double

  def scalarProduct2(xs: Vector[Double], ys: Vector[Double]): Double =
    (xs zip ys).map { case (x, y) => x * y }.sum  //> scalarProduct2: (xs: Vector[Double], ys: Vector[Double])Double

  def scalarProduct3(xs: Vector[Double], ys: Vector[Double]): Double =
    (for ((x, y) <- xs zip ys) yield x * y).sum   //> scalarProduct3: (xs: Vector[Double], ys: Vector[Double])Double

  val v1 = Vector(1.0, 2.0, 3.0)                  //> v1  : scala.collection.immutable.Vector[Double] = Vector(1.0, 2.0, 3.0)
  val v2 = Vector(2.7, 3.2, 4.3)                  //> v2  : scala.collection.immutable.Vector[Double] = Vector(2.7, 3.2, 4.3)

  scalarProduct(v1, v2)                           //> res0: Double = 22.0
  scalarProduct2(v1, v2)                          //> res1: Double = 22.0
  scalarProduct3(v1, v2)                          //> res2: Double = 22.0

  def isPrime(n: Int): Boolean = (2 until n).forall(d => n % d != 0)
                                                  //> isPrime: (n: Int)Boolean
  isPrime(3)                                      //> res3: Boolean = true
  isPrime(4)                                      //> res4: Boolean = false
  isPrime(5)                                      //> res5: Boolean = true

  def primeSum(n: Int) = for {
    i <- 1 until n
    j <- 1 until i
    if isPrime(i + j)
  } yield (i, j)                                  //> primeSum: (n: Int)scala.collection.immutable.IndexedSeq[(Int, Int)]

  def primeSum2(n: Int) =
    (1 until n).flatMap(
      i => (1 until i).withFilter(
        j => isPrime(i + j)).map(
          j => (i, j)))                           //> primeSum2: (n: Int)scala.collection.immutable.IndexedSeq[(Int, Int)]

  primeSum(7)                                     //> res6: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,2
                                                  //| ), (4,1), (4,3), (5,2), (6,1), (6,5))
  primeSum2(7)                                    //> res7: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,2
                                                  //| ), (4,1), (4,3), (5,2), (6,1), (6,5))

}