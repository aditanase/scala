package week6

object poly {
  class Poly(terms0: Map[Int, Double]) {
    def this(bindings: (Int, Double)*) = this(bindings.toMap)
    val terms = terms0 withDefaultValue 0.0
    def +(other: Poly) = new Poly(terms ++ (other.terms map adjust))
    def adjust(term: (Int, Double)): (Int, Double) = {
      val (exp, coeff) = term
      exp -> (coeff + terms(exp))
    }
    override def toString =
      (for ((exp, coeff) <- terms.toList.sorted.reverse)
        yield coeff + "x ^ " + exp) mkString " + "
  }
  
  val p = new Poly(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)  //> p  : week6.poly.Poly = 6.2x ^ 5 + 4.0x ^ 3 + 2.0x ^ 1
}