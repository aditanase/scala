package week5

object lists {
  def lengthFun[T](xs: List[T]): Int =
    (xs foldRight 0)((x, s) => s + 1)             //> lengthFun: [T](xs: List[T])Int

	def concat[T](xs: List[T], ys: List[T]): List[T] =
    (xs foldRight ys) (_ :: _)                    //> concat: [T](xs: List[T], ys: List[T])List[T]
	
	def reverse[T](xs: List[T]): List[T] =
    (xs foldLeft List[T]())((xs, x) => x :: xs)   //> reverse: [T](xs: List[T])List[T]
	
	val l = List(1,2,3,4)                     //> l  : List[Int] = List(1, 2, 3, 4)
	
	lengthFun(l)                              //> res0: Int = 4
	reverse(l)                                //> res1: List[Int] = List(4, 3, 2, 1)
	concat(l,reverse(l))                      //> res2: List[Int] = List(1, 2, 3, 4, 4, 3, 2, 1)
	
}