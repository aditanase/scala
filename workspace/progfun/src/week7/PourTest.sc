package week7

object PourTest {
	val problem = new Pouring(Vector(4,9,19)) //> problem  : week7.Pouring = week7.Pouring@c75e4fc
	problem.moves                             //> res0: scala.collection.immutable.IndexedSeq[Product with Serializable with we
                                                  //| ek7.PourTest.problem.Move] = Vector(Empty(0), Empty(1), Empty(2), Fill(0), Fi
                                                  //| ll(1), Fill(2), Pour(0,1), Pour(0,2), Pour(1,0), Pour(1,2), Pour(2,0), Pour(2
                                                  //| ,1))
	problem.solutions(6)                      //> res1: Stream[week7.PourTest.problem.Path] = Stream(Fill(2) Pour(2,0) Pour(2,
                                                  //| 1)--> Vector(4, 9, 6), ?)
}