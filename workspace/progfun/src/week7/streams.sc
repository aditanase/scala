package week7

object streams {
  def isPrime(n: Int): Boolean = (2 until n).forall(d => n % d != 0)
                                                  //> isPrime: (n: Int)Boolean

  ((1000 to 10000) filter isPrime)(1)             //> res0: Int = 1013
  ((1000 to 10000).toStream filter isPrime)(1)    //> res1: Int = 1013

  def streamRange(lo: Int, hi: Int): Stream[Int] =
    if (lo >= hi) Stream.empty
    else Stream.cons(lo, streamRange(lo + 1, hi)) //> streamRange: (lo: Int, hi: Int)Stream[Int]

  def listRange(lo: Int, hi: Int): List[Int] =
    if (lo >= hi) Nil
    else lo :: listRange(lo + 1, hi)              //> listRange: (lo: Int, hi: Int)List[Int]

  streamRange(3, 10).toList                       //> res2: List[Int] = List(3, 4, 5, 6, 7, 8, 9)

  def from(n: Int): Stream[Int] = n #:: from(n + 1)
                                                  //> from: (n: Int)Stream[Int]
  def sieve(s: Stream[Int]): Stream[Int] =
    s.head #:: sieve(s.tail filter (_ % s.head != 0))
                                                  //> sieve: (s: Stream[Int])Stream[Int]
  val primes = sieve(from(2))                     //> primes  : Stream[Int] = Stream(2, ?)
  (primes take 5).toList                          //> res3: List[Int] = List(2, 3, 5, 7, 11)

  def sqrtStream(x: Double): Stream[Double] = {
    def improve(guess: Double) = (guess + x / guess) / 2
    lazy val guesses: Stream[Double] = 1 #:: (guesses map improve)
    guesses
  }                                               //> sqrtStream: (x: Double)Stream[Double]

  (sqrtStream(2) take 5).toList                   //> res4: List[Double] = List(1.0, 1.5, 1.4166666666666665, 1.4142156862745097, 
                                                  //| 1.4142135623746899)

  def isGoodEnough(guess: Double, x: Double) =
    math.abs((guess * guess - x) / x) < 0.0001    //> isGoodEnough: (guess: Double, x: Double)Boolean
  sqrtStream(2) filter (isGoodEnough(_, 2))       //> res5: scala.collection.immutable.Stream[Double] = Stream(1.4142156862745097
                                                  //| , ?)
	
	from(4) filter (_ < 100) take 10 toList   //> res6: List[Int] = List(4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
	
}