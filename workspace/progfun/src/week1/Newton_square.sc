package week1

object Newton_square {

  def sqrt(x: Double) = {
    def abs(x: Double) = if (x >= 0) x else -x

    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    def improve(guess: Double) =
      (guess + x / guess) / 2

    def isGoodEnough(guess: Double) =
      abs(guess * guess - x) / x < 0.001

    sqrtIter(1.0)
  }                                               //> sqrt: (x: Double)Double
  
  sqrt(2)                                         //> res0: Double = 1.4142156862745097

  sqrt(1e-6)                                      //> res1: Double = 0.0010000001533016628

  sqrt(0.001)                                     //> res2: Double = 0.03162278245070105
  sqrt(0.1e-20)                                   //> res3: Double = 3.1633394544890125E-11
  sqrt(1.0e20)                                    //> res4: Double = 1.0000021484861237E10
  sqrt(1.0e50)                                    //> res5: Double = 1.0000003807575104E25

}