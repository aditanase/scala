package week1

object Lecture {
	// and (x,y) = x && y
	def and(x: Boolean, y: => Boolean): Boolean = if (x) y else false
                                                  //> and: (x: Boolean, y: => Boolean)Boolean
	
	// or (x,y) = x || y
	def or(x: Boolean, y: => Boolean): Boolean = if (x) true else y
                                                  //> or: (x: Boolean, y: => Boolean)Boolean
	
	and(true, false)                          //> res0: Boolean = false
	or(false, false)                          //> res1: Boolean = false
	
	
	// non-tail recursive factorial
	def factorial(n: Int): Int = {
		if (n==0) 1 else n * factorial(n-1)
	}                                         //> factorial: (n: Int)Int
	
	factorial(4)                              //> res2: Int = 24

	// a tail recursive version of factorial
	
	def factorial2(n: Int): Int = {
		def fact(x: Int, y: Int):Int = {
			if (x==0) y else fact(x-1, x*y)
		}
		
		fact(n, 1)
	}                                         //> factorial2: (n: Int)Int
	
	factorial2(5)                             //> res3: Int = 120

}