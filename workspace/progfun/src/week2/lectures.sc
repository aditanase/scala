package week2

object lectures {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  // sum, written using currying
  def sum(f: Int => Int)(a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a+1, acc + f(a))
    }
    loop(a, 0)
  }                                               //> sum: (f: Int => Int)(a: Int, b: Int)Int
  
 	sum(x => x * x)(3,5)                      //> res0: Int = 50
  def sumId = sum(x=>x)_                          //> sumId: => (Int, Int) => Int
  sumId(3,5)                                      //> res1: Int = 12
  
  // product, written using currying as regular recursive
  def product(f: Int => Int)(a: Int, b: Int): Int = {
  	def p = product(f)_
    if (a>b) 1 else f(a) * p(a+1, b)
  }                                               //> product: (f: Int => Int)(a: Int, b: Int)Int
  // product, written using currying as tail recursive
  def productr(f: Int => Int)(a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a+1, acc * f(a))
    }
    loop(a, 1)
  }                                               //> productr: (f: Int => Int)(a: Int, b: Int)Int
	  
 	def p1 = product(x => x * x)_             //> p1: => (Int, Int) => Int
  p1(3,5)                                         //> res2: Int = 3600
 	def p2 = productr(x => x * x)_            //> p2: => (Int, Int) => Int
  p2(3,5)                                         //> res3: Int = 3600
  
  // define factorial in terms of product
  def factorial(n: Int): Int = product(x=>x)(1, n)//> factorial: (n: Int)Int
  
  factorial(3)                                    //> res4: Int = 6
  println("test")                                 //> test
  
  // generalizing sum and prod
  def generate(f: Int => Int, unit: Int, op: (Int,Int) => Int)(a: Int, b: Int): Int = {
    if (a>b) unit else op(f(a), generate(f, unit, op)(a+1, b))
  }                                               //> generate: (f: Int => Int, unit: Int, op: (Int, Int) => Int)(a: Int, b: Int)
                                                  //| Int
  
  def sumg = generate(x=>x*x, 0, (x,y)=>x+y)_     //> sumg: => (Int, Int) => Int
  def prodg = generate((x=>x*x), 1, (x,y)=>x*y)_  //> prodg: => (Int, Int) => Int
  
  sumg(3,5)                                       //> res5: Int = 50
  prodg(3,5)                                      //> res6: Int = 3600

 }