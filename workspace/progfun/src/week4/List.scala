package week4

object List {
	// List(1, 2) = List.apply(1, 2)
	def apply[T](x1: T, x2: T): List[T] = new Cons(x1, new Cons(x2, Nil))
	def apply[T]() = Nil
	def apply[T](x: T): List[T] = new Cons(x, Nil)
}

trait List[+T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
  def prepend[U >: T](elem: U): List[U] = new Cons(elem, this)
}
class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty = false
  override def toString = "(" + head + ") -> " + tail
}
object Nil extends List[Nothing] {
  def isEmpty = true
  def head = throw new NoSuchElementException("Nil.head")
  def tail = throw new NoSuchElementException("Nil.tail")
  override def toString = "Nil"
}
