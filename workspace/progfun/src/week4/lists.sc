package week4

object lists {
  val l = new Cons(1, new Cons(2, Nil))           //> l  : week4.Cons[Int] = (1) -> (2) -> Nil
  val l2 = List.apply(10,20)                      //> l2  : week4.List[Int] = (10) -> (20) -> Nil
  val l0 = List()                                 //> l0  : week4.Nil.type = Nil
  val l1 = List(40)                               //> l1  : week4.List[Int] = (40) -> Nil
  l1 prepend l0                                   //> res0: week4.List[Any] = (Nil) -> (40) -> Nil
  //def f(xs: List[NonEmpty], x: Empty) = xs prepend x
}