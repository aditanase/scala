package week4

object pat {
  // EXPRESSIONS
  val s = Sum(Number(1), Number(2))               //> s  : week4.Sum = Sum(Number(1),Number(2))
  s.eval                                          //> res0: Int = 3
  s.show                                          //> res1: String = 1 + 2

  val p = Prod(Number(3), Number(4))              //> p  : week4.Prod = Prod(Number(3),Number(4))
  p.eval                                          //> res2: Int = 12
  p.show                                          //> res3: String = 3 * 4

  val e1 = Sum(Prod(Number(3), Var("x")), Var("y"))
                                                  //> e1  : week4.Sum = Sum(Prod(Number(3),Var(x)),Var(y))
  e1.show                                         //> res4: String = 3 * x + y

  val e2 = Prod(Sum(Number(3), Var("x")), Var("y"))
                                                  //> e2  : week4.Prod = Prod(Sum(Number(3),Var(x)),Var(y))
  e2.show                                         //> res5: String = (3 + x) * y


}