package simulations

import math.random
import scala.util.Random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8

    // to complete: additional parameters of simulation
    val percentInfected: Int = 1
    val airTraffic = false
    val mobilityAct = false
    val chosenFew = 0 // percent
  }

  import SimConfig._

  def personList (count: Int): List[Person] = {
    require (count > 0)
    
    (1 to count).toList.map(x => new Person(x))
    
  }
  
  val persons: List[Person] = personList(population) // to complete: construct list of persons

  class Person (val id: Int) {
    var infected = false
    var sick = false
    var immune = false
    var dead = false

    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)

    // kickoff simulation - 1% start sick, everyone starts to move    
    if (id <= percentInfected * population / 100) getInfected
    
    // chosen few act - 5% are vaccinated
    if (randomBelow(100) < chosenFew && !infected) {
      immune = true
    }
    move

    // moves every live person every 1..5 days
    def move {
      
      afterDelay(mobility) {
        if (!dead) {
          if (airTraffic && randomBelow(100) < 1) fly
          else moveToNextRoom
          // 40% chance to get sick in the new room
          if (infectedRoom(row, col) && randomBelow(100) < 40) getInfected
          move
        }
      }
      
      def mobility: Int = {
        var days = randomBelow(5) + 1
        if (mobilityAct) {
          days = days * 2
          if (sick) days = days * 2
        }
        days
      }
      
      def fly {
	    row = randomBelow(roomRows)
	    col = randomBelow(roomColumns)
      }

      // checks if room has any contagious (infected) people
      def infectedRoom(r: Int, c: Int) = persons.exists(p => p.row == r && p.col == c && p.infected)

      // checks if room has any visibley infected (sick or dead) people
      def visiblyInfectedRoom(r: Int, c: Int) = persons.exists(p => p.row == r && p.col == c && (p.sick || p.dead))

      // goes through all 4 adjacent rooms until we find one w/o visibly infected people
      // if all are infected, person does not move
      def moveToNextRoom {
        def newRoom(r: Int, c: Int) = {
          val nr = (row + r + roomRows) % roomRows
          val nc = (col + c + roomColumns) % roomColumns
          (nr, nc)
        }

        // generates random list of adjacent rooms that are safe to move to
        Random.shuffle((1 to 4).toList) map {
          case 1 => newRoom(-1, 0)
          case 2 => newRoom(0, 1)
          case 3 => newRoom(1, 0)
          case 4 => newRoom(0, -1)
        } filterNot {
          room => visiblyInfectedRoom(room._1, room._2)
        } match {
          // move to first safe room or stay put
          case (r, c) :: xs =>
            row = r
            col = c
          case _ =>
        }

      }
    }
       
    def getInfected {
      if (!(infected || immune)) {
        infected = true
        
        afterDelay(6) { getSick }
        
        afterDelay(14) {
          if (randomBelow(100) < 25)  die
          else afterDelay(2) { getImmune }
        }
      }
    }
    
    def getSick {
      sick = true
    }
    def die {
      dead = true
    }
    def getImmune {
      if (!dead) {
	      sick = false
	      immune = true
	      
	      afterDelay(2) { getHealthy }
      }
    }
    def getHealthy {
      if (!dead) {
        infected = false
        immune = false
      }
    }
  }
}