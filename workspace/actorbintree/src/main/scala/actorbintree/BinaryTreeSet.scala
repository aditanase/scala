/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case GC => {
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot))
    }
    case op: Operation => root ! op
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case CopyFinished => {
      context.stop(root)
      root = newRoot

      pendingQueue foreach (root ! _)
      pendingQueue = Queue.empty[Operation]

      context.parent ! CopyFinished
      context.become(normal)
    }
    case op: Operation => pendingQueue = pendingQueue enqueue op
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  def subtreeContains(pos: Position, requester: ActorRef, id: Int, elem: Int) = subtrees.get(pos) match {
    case Some(tree) => tree ! Contains(requester, id, elem)
    case None => requester ! ContainsResult(id, false)
  }

  def insertSubtree(pos: Position, requester: ActorRef, id: Int, elem: Int) = subtrees.get(pos) match {
    case Some(tree) => tree ! Insert(requester, id, elem)
    case None => {
      subtrees = subtrees + (pos -> context.actorOf(BinaryTreeNode.props(elem, initiallyRemoved = false)))
      requester ! OperationFinished(id)
    }
  }

  def removeSubtree(pos: Position, requester: ActorRef, id: Int, elem: Int) = subtrees.get(pos) match {
    case Some(tree) => tree ! Remove(requester, id, elem)
    case None => requester ! OperationFinished(id)
  }

  def forward(operation: (Position, ActorRef, Int, Int) => Unit, requester: ActorRef, id: Int, elem: Int) = {
    if (elem < this.elem) {
      operation(Left, requester, id, elem)
    } else {
      operation(Right, requester, id, elem)
    }
  }

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case Contains(requester, id, elem) => {
      if (this.elem == elem) {
        requester ! ContainsResult(id, !this.removed)
      } else {
        forward(subtreeContains, requester, id, elem)
      }
    }

    case Insert(requester, id, elem) => {
      if (this.elem == elem) {
        removed = false
        requester ! OperationFinished(id)
      } else {
        forward(insertSubtree, requester, id, elem)
      }
    }

    case Remove(requester, id, elem) => {
      if (this.elem == elem) {
        removed = true
        requester ! OperationFinished(id)
      } else {
        forward(removeSubtree, requester, id, elem)
      }
    }

    case CopyTo(newRoot) => {
      if (!removed) newRoot ! Insert(self, 0, elem)
      subtrees.values foreach (_ ! CopyTo(newRoot))
      context.become(copying(subtrees.values.toSet, removed))
    }

  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    if (expected.isEmpty && insertConfirmed) {
      context.parent ! CopyFinished
      normal
    } else {
      case OperationFinished(_) => context.become(copying(expected, true))
      case CopyFinished =>         context.become(copying(expected - sender, insertConfirmed))
    }
  }

}
