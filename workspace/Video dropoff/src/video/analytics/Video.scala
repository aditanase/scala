import scala.collection.mutable.Map 
import collection.immutable.SortedMap

package video.analytics {
	
	class VideoSegment (f: Int, t: Int) extends Ordered[VideoSegment] {
		require (t>=f && f >=0 && t >= 0)
		val from: Int = f
		val to: Int = t

		def this(timeSpent: Int) = this(0, timeSpent)
		def length:Int = to - from

		override def toString = "[" + from + ":" + to + "]"
		override def equals(that: Any): Boolean = 
			that.isInstanceOf[VideoSegment] && 
			this.from == that.asInstanceOf[VideoSegment].from && 
			this.to == that.asInstanceOf[VideoSegment].to
		override def hashCode = toString.hashCode
		
		// the segment most to the right is "biggest"
		def compare(that: VideoSegment) = 
			if (this.to != that.to) this.to - that.to else this.from - that.from
	}

	class VideoMap (segmentList: VideoSegment*) {

		protected val segments = Map[VideoSegment, Int]()
		for (s <- segmentList) addSegment(s)

		def addSegment (s: VideoSegment) {
			if (segments.contains(s))
				segments(s) += 1
			else 
				segments += (s -> 1)
		}

		def sortedSegments = SortedMap(segments.toSeq:_*)
		def maxTime: Int = segments.keySet.max.to

		// brute force implementation, cycle through all minutes from all segments
		def naiveGraph: List[Int] = {
			val audience = new Array[Int](maxTime + 2)

			// add all segment minutes to the overall minute audience
			for ((s, v) <- segments)
				for (i <- s.from to s.to)
					audience(i) += v
			
			audience.toList
		}
		
		// optimized implementation, goes up and down on the chart as people join or leave
		def smartGraph: List[Int] = {
			val audienceDynamic = new Array[Int](maxTime + 2)
			
			for ((s, v) <- segments) {
				audienceDynamic(s.from) += v    // increase when people join
				audienceDynamic(s.to + 1) -= v  // decrease after they leave
			}

			// transform audience dynamics into absolute minute audience
			for (i <- 1 until audienceDynamic.length)
				audienceDynamic(i) += audienceDynamic(i-1)

			audienceDynamic.toList
		}

		override def toString = segments.toString

		def graphToString(graph: List[Int]):String = {
			val lines = for (y <- 1 to graph.max)
				yield {
					val chars = for (x <- graph)
						yield {
							if (x>=y) "#" else " "
						}
					val padding = graph.max.toString.length - y.toString.length
					y + " " * padding + " | " + chars.mkString
				}
			lines.reverse.mkString("\n")

		}

		def printSession {
			println("\nFlat session: " + sortedSegments)
			println("Brute force: " + naiveGraph)
			println("Optimized:   " + smartGraph)
			println("\n" + graphToString(smartGraph))
		}

	}

	// map-reduce implementation, explodes map into tuples then reduces them into an array
	trait MapReduce extends VideoMap {
		override def smartGraph: List[Int] = {
			val audienceDynamic = new Array[Int](maxTime + 2)
			
			val list = segments.toList.flatMap { 
				case(s, v) => List ( (s.from, v), (s.to  + 1, -v) )
			}

			val k = list.groupBy(x=>x._1).mapValues( y => y.map ( z => z._2))

			val j = (0 to k.keys.max).flatMap(x => k.get(x)).map( y => y.foldLeft(0) { _ + _ })
			// val j = (0 to k.keys.max).map(x => k.getOrElse(x, ()))
			//val j = (0 to k.keys.max).map(x => k.withDefaultValue(())(x))

			println(j)

			for ((i, v) <- list) {
				audienceDynamic(i) += v    // increase when people join or decrese when they leave
			}

			// transform audience dynamics into absolute minute audience
			for (i <- 1 until audienceDynamic.length)
				audienceDynamic(i) += audienceDynamic(i-1)

			audienceDynamic.toList
		}
	}

}
