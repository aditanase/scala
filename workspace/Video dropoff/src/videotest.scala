import video.analytics.MapReduce
import video.analytics.VideoMap
import video.analytics.VideoSegment

object VideoTest extends App {

	// simulate simple sessions
	val session = new VideoMap() with MapReduce
	val timeSpent = Array(8,5,2,6,9,4,2,6,2,2,4,1)

	for (t <- timeSpent) session.addSegment(new VideoSegment(t))
	session.addSegment(new VideoSegment(0))
	session.addSegment(new VideoSegment(25,50))

	session.printSession
}