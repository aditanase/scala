package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("min2") = forAll { (a: Int, b: Int) =>
    val h1 = insert(a, empty)
    val h2 = insert(b, empty)
    findMin(meld(h1, h2)) == (a min b)
  }
  
  property("del1") = forAll { (a: Int) =>
    isEmpty(deleteMin(insert(a, empty)))
  }  
  property("del2") = forAll { (a: Int, b: Int) =>
    val h1 = insert(a, empty)
    val h2 = insert(b, empty)
    findMin(deleteMin(meld(h1, h2))) == (a max b)
  }

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  def toList(h: H): List[Int] = if(isEmpty(h)) Nil else findMin(h) :: toList(deleteMin(h))
   
  def fromList(l: List[Int]): H = l match {
    case x :: xs => insert(x, fromList(xs))
    case Nil => empty
  }

  property("gen2") = forAll { (h: H) =>
  	val l = toList(h)
  	val g = fromList(l.reverse) 
  	l == toList(g)
  }

  lazy val genHeap: Gen[H] = for {
    i <- arbitrary[Int]
    m <- oneOf(value(empty), genHeap)
  } yield insert(i, m)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
