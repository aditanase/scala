package patmat

import Huffman._

object Huff {

  times("text".toList)                            //> res0: List[(Char, Int)] = List((t,2), (x,1), (e,1))

  val t1 = Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5)
                                                  //> t1  : patmat.Huffman.Fork = Fork(Leaf(a,2),Leaf(b,3),List(a, b),5)
  val t2 = Fork(Fork(Leaf('a', 2), Leaf('b', 3), List('a', 'b'), 5), Leaf('d', 4), List('a', 'b', 'd'), 9)
                                                  //> t2  : patmat.Huffman.Fork = Fork(Fork(Leaf(a,2),Leaf(b,3),List(a, b),5),Leaf
                                                  //| (d,4),List(a, b, d),9)
 
	encode(createCodeTree("text".toList))("text".toList)
                                                  //> res1: List[patmat.Huffman.Bit] = List(1, 0, 1, 0, 0, 1)
	println(
	    decode(
	        createCodeTree("text".toList),
	        List(1,0,1,0,0,1)
	    ).mkString
	)                                         //> text
	println(decode(frenchCode, secret).mkString)
                                                  //> huffmanestcool
	}