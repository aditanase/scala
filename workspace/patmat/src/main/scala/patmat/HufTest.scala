package patmat
import Huffman._

object HufTest extends App {
	println(encode(createCodeTree("text".toList))("text".toList))
	println(quickEncode(createCodeTree("text".toList))("text".toList))
	
	println(
	    decode(
	        createCodeTree("text".toList), 
	        List(1,0,1,0,0,1)
	    ).mkString
	) 
	 
	println(decode(frenchCode, secret).mkString)
	val frenchSecret = quickEncode(frenchCode)(decode(frenchCode, secret))
	
	println(decode(frenchCode, frenchSecret).mkString)
}