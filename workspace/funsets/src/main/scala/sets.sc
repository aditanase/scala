object sets {
  import funsets.FunSets._
  println(contains(singletonSet(1), 1))           //> true
    
  val s1:Set = x => (x > -3 && x<10 && x%2==0)    //> s1  : Int => Boolean = <function1>
  val e:Set = x => false                          //> e  : Int => Boolean = <function1>
  printSet(s1)                                    //> {-2,0,2,4,6,8}
  println(forall(s1, x => x < 7))                 //> false
  println(exists(s1, x => x > 10))                //> false
  printSet(map(s1, x=> x - 1))                    //> {-3,-1,1,3,5,7}
  println(forall(map(s1, x=> x * 2), x => x%2==0))//> true
}