package nodescala
import scala.language.postfixOps
import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global
import scala.async.Async.{async, await}

object Run extends App {
    println("start")
    
	val working = Future.run() { ct =>
	  Future {
	    while (ct.nonCancelled) {
	      println("working")
	    }
	    println("done")
	  }
	}                            
	Future.delay(2 seconds) onSuccess {
	  case _ => {
		println("no more")
	    working.unsubscribe()
	  }
	}
    println("end")
}