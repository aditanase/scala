package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    require (r>=0 && c>=0 && c<=r)
    
    if (r==0 || c==0 || c==r) 1
    else pascal(c-1, r-1) + pascal (c, r-1)
  
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {

    // keep a stack of opening and closing parantheses
    def checkBalance(i: Int, xs: List[Char]): Boolean = {
      if (i<0) false
      else xs match {
        case Nil => i==0
        case y :: ys => 
          y match {
            case '(' => checkBalance(i+1, ys) 
            case ')' => checkBalance(i-1, ys) 
            case _   => checkBalance(i, ys) 
          }
      }
    }
    // optimize by stripping out chars that are not ( or )
    checkBalance(0, chars.filter(Set('(', ')').contains(_)))
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def countChangeSorted(m: Int, c: List[Int]): Int = {
	    if (m <= 0) 0
	    else if (c.isEmpty) 0
	    else if (m == c.head) 1
	    else countChangeSorted(m-c.head, c) + countChangeSorted(m, c.tail)
    }
    countChangeSorted(money, coins.sorted)
  }
}
