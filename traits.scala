trait HasLegs extends Animal {
	override def toString = "I am a " + this.name + " and I have legs"
}

class Animal(def name:String) {
	override def toString = "I am a " + this.name + " and I am alive"
}

var wolf = new Animal("Wolf") with HasLegs

println(wolf)