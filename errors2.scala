import scala.io.Source

def extractErrors(fileName: String) = {
	for {
		line <- Source.fromFile(fileName).getLines() 
		if line.startsWith("static")
		words = line.split(" ")
	} yield (words(5).dropRight(1).toInt, words(3).split(":")(0)) // code, mess
}

def codetemplate(code:Int, mess: String) = {
	"\t\t\t{\n" +
	"\t\t\t\t\"code\": \"" + code + "\",\n" +
	"\t\t\t\t\"message\": \"" + mess + "\",\n" +
	"\t\t\t},"
}

if (args.length > 0) {
	for ((errorCode, errorMessage) <- extractErrors(args(0)))
		println(codetemplate(errorCode, errorMessage))
} else {
	println("Please enter file name")
}