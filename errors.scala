// some maps too
import scala.collection.mutable.Map 
val errorMap = Map[String, Int]()

// Player Error Codes for RTA
errorMap += ("FATAL" -> 1000)
errorMap += ("CRITICAL" -> 2000)
errorMap += ("NON_CRITICAL" -> 3000)

// FATAL
errorMap += ("EVENT_CONFIG_SERVICE_IO_ERROR" -> 100)
errorMap += ("EVENT_CONFIG_SERVICE_SECURITY_ERROR" -> 200)
errorMap += ("EVENT_CONFIG_SERVICE_PARSE_ERROR" -> 300)
errorMap += ("EVENT_CONFIG_SERVICE_NO_CONTENT" -> 400)
errorMap += ("EVENT_CONFIG_SERVICE_NO_SOURCE_PATH" -> 500)

errorMap += ("MEDIA_ERROR" -> 900)

errorMap += ("LIVE_SOURCES_SERVICE_IO_ERROR" -> 101)
errorMap += ("LIVE_SOURCES_SERVICE_SECURITY_ERROR" -> 201)
errorMap += ("LIVE_SOURCES_SERVICE_PARSE_ERROR" -> 301)
errorMap += ("LIVE_SOURCES_SERVICE_NO_CONTENT" -> 401)
errorMap += ("LIVE_SOURCES_SERVICE_NO_SOURCE_PATH" -> 501)


// CRITICAL

errorMap += ("SECONDARY_LIVE_SOURCES_SERVICE_PARSE_ERROR" -> 1301)
errorMap += ("SECONDARY_LIVE_SOURCES_SERVICE_NO_CONTENT" -> 1302)

errorMap += ("EVENT_STATUS_SERVICE_IO_ERROR" -> 1103)
errorMap += ("EVENT_STATUS_SERVICE_SECURITY_ERROR" -> 1203)
errorMap += ("EVENT_STATUS_SERVICE_PARSE_ERROR" -> 1303)
errorMap += ("EVENT_STATUS_SERVICE_NO_CONTENT" -> 1401)
errorMap += ("EVENT_STATUS_SERVIC_NO_SOURCE_PATH" -> 1501)

errorMap += ("VOD_SOURCES_SERVICE_IO_ERROR" -> 1105)
errorMap += ("VOD_SOURCES_SERVICE_SECURITY_ERROR" -> 1205)
errorMap += ("VOD_SOURCES_SERVICE_PARSE_ERROR" -> 1305)
errorMap += ("VOD_SOURCES_SERVICE_NO_CONTENT" -> 1405)
errorMap += ("VOD_SOURCES_SERVICE_NO_SOURCE_PATH" -> 1505)

errorMap += ("NOTIFICATION_SERVICE_IO_ERROR" -> 1106)
errorMap += ("NOTIFICATION_SERVICE_SECURITY_ERROR" -> 1206)
errorMap += ("NOTIFICATION_SERVICE_PARSE_ERROR" -> 1306)
errorMap += ("NOTIFICATION_SERVICE_NO_CONTENT" -> 1406)
errorMap += ("NOTIFICATION_SERVICE_NO_SOURCE_PATH" -> 1507)

errorMap += ("AFFILIATES_SERVICE_IO_ERROR" -> 1107)
errorMap += ("AFFILIATES_SERVICE_SECURITY_ERROR" -> 1207)
errorMap += ("AFFILIATES_SERVICE_PARSE_ERROR" -> 1307)
errorMap += ("AFFILIATES_SERVICE_NO_CONTENT" -> 1407)
errorMap += ("AFFILIATES_SERVICE_NO_SOURCE_PATH" -> 1507)

errorMap += ("MIDROLLS_SERVICE_IO_ERROR" -> 1113)
errorMap += ("MIDROLLS_SERVICE_SECURITY_ERROR" -> 1213)
errorMap += ("MIDROLLS_SERVICE_PARSE_ERROR" -> 1313)
errorMap += ("MIDROLLS_SERVICE_NO_CONTENT" -> 1413)
errorMap += ("MIDROLLS_SERVICE_NO_SOURCE_PATH" -> 1513)


errorMap += ("NOTIFY_PAGE_ERROR" -> 1409)
errorMap += ("NOTIFY_PLAYER_ERROR" -> 1410)
errorMap += ("NOTIFY_PLAYER_UNEXPECTED_EVENT_STATUS" -> 1411)

errorMap += ("AUDITUDE_NO_CONFIG" -> 1611)

// NON CRITICAL
errorMap += ("SHARE_TARGETS_SERVICE_IO_ERROR" -> 2104)
errorMap += ("SHARE_TARGETS_SERVICE_SECURITY_ERROR" -> 2204)
errorMap += ("SHARE_TARGETS_SERVICE_PARSE_ERROR" -> 2304)
errorMap += ("SHARE_TARGETS_SERVICE_NO_CONTENT" -> 2404)
errorMap += ("SHARE_TARGETS_NO_SOURCE_PATH" -> 2504)

errorMap += ("TIMELINE_MARKERS_SERVICE_IO_ERROR" -> 2102)
errorMap += ("TIMELINE_MARKERS_SERVICE_SECURITY_ERROR" -> 2202)
errorMap += ("TIMELINE_MARKERS_SERVICE_PARSE_ERROR" -> 2302)
errorMap += ("TIMELINE_MARKERS_SERVICE_NO_CONTENT" -> 2402)
errorMap += ("TIMELINE_MARKERS_NO_SOURCE_PATH" -> 2502)

errorMap += ("DMA_GEO_SERVICE_IO_ERROR" -> 2108)
errorMap += ("DMA_GEO_SERVICE_SECURITY_ERROR" -> 2208)
errorMap += ("DMA_GEO_SERVICE_PARSE_ERROR" -> 2308)
errorMap += ("DMA_GEO_SERVICE_NO_CONTENT" -> 2408)
errorMap += ("DMA_GEO_SERVICE_NO_SOURCE_PATH" -> 2508)

errorMap += ("VIDEO_PIXEL_LOAD_FAULT" -> 2613)

errorMap += ("AKAMAI_NO_CONFIG" -> 2812)

/*
thousands for error level 
	fatal (0-999) 
	critical (1000-1999) 
	non-critical (2000-2999)
hundreds for error code
	IOError 100
	SecurityError 200
	Invalid JSON 300
	communication 400
	no source path 500
	ads 600
	pixel tracking 700
	akamai 800
	media error 900
tens for details
	event config 00
	livesources 01
	timelinemarkers 02
	event status 03
	sharetargets 04
	vod sources	05
	notifications 06
	affiliates 07
	dmaGeoService 08
	notifyPage 09
	notifyPlayer 10
	unexpectedEventStatus 11
	no config 12
	pixel load fault 13
midrolls service 14
*/


//println (errorMap.keys.toArray.length)

def codetemplate(code:Int, mess: String) = {
	"\t\t\t{\n" +
	"\t\t\t\t\"code\": \"" + code + "\",\n" +
	"\t\t\t\t\"message\": \"" + mess + "\",\n" +
	"\t\t\t},"
}

// println(codetemplate(2, "test"))

for ((errorMessage, errorCode) <- errorMap)
	println(codetemplate(errorCode,errorMessage))
