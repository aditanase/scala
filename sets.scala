import scala.collection.mutable.Set

val planes = Set("boeing", "airbus")
planes += "Cessna"

// println(planes)
// println(planes.contains("douglas"))

// some maps too
import scala.collection.mutable.Map 
val treasureMap = Map[Int, String]()
treasureMap += (1 -> "go to island")
treasureMap += (2 -> "find big x on ground")
treasureMap += (3 -> "dig")
println (treasureMap(2))


