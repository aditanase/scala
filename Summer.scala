import ChecksumAccumulator.calculate

object Summer {
	def main (args: Array[String]) {
		for (arg <- args)
			println (arg + ": " + calculate(arg))
	}
}

object Winter extends App {
	for (season <- Array("spring", "summer", "autumn", "winter"))
		println (season + ": " + calculate(season))
}

object Spring extends App {
	for (season <- 500 to 510)
		println (season + ": " + calculate(season.toString))
}

