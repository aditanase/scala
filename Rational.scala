class Rational (n: Int, d: Int) {
	// implicit constructor code
	require (d!=0)
	private val g = gcd(n.abs, d.abs)
	val numer = n/g
	val denom = d/g

	// overloaded constructor for integers
	def this (n: Int) = this (n,1)

	// operators for rational and ints
	def + (that: Rational): Rational = 
		new Rational(
			numer * that.denom + denom * that.numer,
			denom * that.denom
		)
	def + (i: Int): Rational =
		new Rational(numer + denom * i, denom)

	def - (that: Rational): Rational = 
		new Rational(
			numer * that.denom - denom * that.numer,
			denom * that.denom
		)
	def - (i: Int): Rational =
		new Rational(numer - denom * i, denom)

	def * (that: Rational): Rational = 
		new Rational (numer * that.numer, denom * that.denom)
	def * (i: Int): Rational = 
		new Rational (numer * i, denom)

	def / (that: Rational): Rational = 
		new Rational (numer * that.denom, denom * that.numer)
	def / (i: Int): Rational = 
		new Rational (numer, denom * i)

	override def toString = numer + "/" + denom

	private def gcd(a: Int, b: Int): Int =
		if (b==0) a else gcd (b, a%b)
}

implicit def intToRational(x: Int) = new Rational(x)

var a = new Rational (1,2)
var b = new Rational (3,4)
var c = 1

println ("Rational operations")
println (a + b)
println (a - b)
println (a * b)
println (a / b)

println ("Rational/int operations")
println (c + b)
println (c - b)
println (c * b)
println (c / b)
