val thrill = "Will" :: "Fill" :: "Until" :: Nil

println (thrill)

println (thrill.count(s => s.length == 4))

println (thrill.drop(2))

println (thrill.exists (s => s == "Until"))

println (thrill.filter(s => s.length == 4))

println (thrill.reverse)