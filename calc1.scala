
if (args.length>0) {
	val maxTime = args(0).toInt
	var combinations = 0
	for (i <- 1 to maxTime)
		for (j <- i to maxTime)
			combinations += 1

	println(combinations)
}